var express = require('express'),
    config = require('./config/config'),
    glob = require('glob'),
    mongoose = require('mongoose');

mongoose.connect(config.db);
var db = mongoose.connection;
db.on('error', function () {
  throw new Error('unable to connect to database at ' + config.db);
});

var models = glob.sync(config.root + '/app/models/*.js');
models.forEach(function (model) {
  require(model);
});
var app = express();

require('./config/express')(app, config);

app.listen(config.port, function () {
  console.log('Express server listening on port ' + config.port);
});

function getDateTime() {

  var date = new Date();

  var hour = date.getHours();
  hour = (hour < 10 ? "0" : "") + hour;

  var min  = date.getMinutes();
  min = (min < 10 ? "0" : "") + min;

  var sec  = date.getSeconds();
  sec = (sec < 10 ? "0" : "") + sec;

  var year = date.getFullYear();

  var month = date.getMonth() + 1;
  month = (month < 10 ? "0" : "") + month;

  var day  = date.getDate();
  day = (day < 10 ? "0" : "") + day;

  return year + ":" + month + ":" + day + ":" + hour + ":" + min + ":" + sec;
}

// GPIO
var Gpio = require('onoff').Gpio,
    led = new Gpio(21, 'out'),
    button = new Gpio(20, 'in', 'both'),
    led2 = new Gpio(16, 'out');

// Socket.io Server
var socketServer = app.listen(4200, function () {
      console.log('Socket.io server listening on port 4200');
    }),
    io = require('socket.io')(socketServer);

// Set socket.io listeners.
io.on('connection', function(client) {
  console.log('Client connected...');
  led2.writeSync(1);
  console.log('reading button status:', button.readSync());
  client.emit('hardware-event', {'door': button.readSync(), 'time': getDateTime()});
  client.on('join', function(data) {
    console.log(data);
  });
  client.on('disconnect', function() {
    led2.writeSync(0);
    console.log('user disconnected');
  });
});

button.watch(function(err, value) {
  if (value) {
    console.log('Door is OPEN');
    led.writeSync(1);
  } else {
    console.log('Door is CLOSED');
    led.writeSync(0);
  }
  io.emit('hardware-event', {'door': value, 'time': getDateTime()});
});

function exit() {
  console.log('Server shutting down...');
  led.unexport();
  button.unexport();
  process.exit();
}
process.on('SIGINT', exit);
process.on('SIGTERM', exit);
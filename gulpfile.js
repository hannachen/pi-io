var gulp = require('gulp'),
    nodemon = require('gulp-nodemon'),
    plumber = require('gulp-plumber'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    cssmin = require('gulp-cssmin'),
    livereload = require('gulp-livereload'),
    sass = require('gulp-ruby-sass');

gulp.task('sass', function () {
  return sass('app/src/styles/**/*.scss')
    .pipe(concat('main.min.css'))
    .pipe(gulp.dest('public/css'))
    .pipe(livereload());
});

// Concatenate & Minify JS
gulp.task('js:vendor', function() {
  return gulp.src([
      'bower_components/jquery/dist/jquery.js',
      'bower_components/lodash/lodash.js',
      'bower_components/socket.io-client/socket.io.js'
    ])
    .pipe(concat('vendor.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('public/js'))
    .pipe(livereload());
});
gulp.task('js:main', function() {
  return gulp.src('app/src/scripts/*.js')
    .pipe(concat('main.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('public/js'))
    .pipe(livereload());
});

gulp.task('scripts', [
  'js:vendor',
  'js:main'
]);

gulp.task('watch', function() {
  gulp.watch('./public/css/*.scss', ['sass']);
});

gulp.task('develop', function () {
  livereload.listen();
  nodemon({
    script: 'app.js',
    ext: 'js coffee handlebars',
    stdout: false
  }).on('readable', function () {
    this.stdout.on('data', function (chunk) {
      if(/^Express server listening on port/.test(chunk)){
        livereload.changed(__dirname);
      }
    });
    this.stdout.pipe(process.stdout);
    this.stderr.pipe(process.stderr);
  });
});

gulp.task('build', [
  'scripts',
  'sass'
]);

gulp.task('default', [
  'sass',
  'develop',
  'watch'
]);

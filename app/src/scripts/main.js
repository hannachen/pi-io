'use strict';
(function($, window) {

  var PiServer = {
    init: function() {
      this.initVariables();
      this.initEvents();
      this.initPage();
    },
    initVariables: function() {
      this.$document = $(document);
      this.$status = $('#status');
      this.$logs = $('#logs');
      this.socket = io.connect('http://' + window.location.hostname + ':4200');
    },
    initEvents: function() {
      var _this = this;
      this.socket.on('connect', function(data) {
        console.log('connected', data);
      });
      this.socket.on('hardware-event', function(data) {
	var status = data.door ? 'open' : 'close',
	    time = data.time,
	    $logEntry = $('<li>' + status + ' - ' + time + '</li>');
	_this.$status.removeClass().addClass(status);
	console.log($logEntry);
	$logEntry.appendTo(_this.$logs);
        console.log('hardware event:', data);
      });
    },
    initPage: function() {
    }
  }

  $(function() {
    PiServer.init();
  });
})(jQuery, window);
